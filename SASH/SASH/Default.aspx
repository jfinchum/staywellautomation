﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SASH._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Automation Dashboard</h1>

        <p><a href="" class="btn btn-primary btn-lg">Run Tests &raquo;</a></p>
    </div>
    
    <div class="row">
        <div class="col-md-4">
            <h2>Queued Tests</h2>
            <p>
                <asp:Table ID="tblQueuedTests" runat="server" GridLines="Both" CellPadding="4" CssClass="yui-grid">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell>
                                                Test Name
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                Initiated By
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                Queued On
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                Browser
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                OS
                        </asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
            </p>
        </div>
                <div class="col-md-4">
            <h2>Running Tests</h2>
            <p>
                <asp:Table ID="tblRunningTests" runat="server" GridLines="Both" CellPadding="4" CssClass="yui-grid">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell>
                                                Test Name
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                Initiated By
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                Running For
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                Test Environment
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                Test Machine
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                Browser
                        </asp:TableHeaderCell>
                        <asp:TableHeaderCell>
                                                OS
                        </asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>
            </p>
        </div>
    </div>
</asp:Content>
