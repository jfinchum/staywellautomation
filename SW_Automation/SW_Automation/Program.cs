﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using SharedControls;

namespace SW_Automation
{
    class Program : PublicMethods
    {
        static void Main(string[] args)
        {
            RunCode RC = new RunCode();
            RunFlatFile RF = new RunFlatFile();
            WindowControl WinControl = new WindowControl();

            bool Debug = Convert.ToBoolean(ConfigurationManager.AppSettings["DebugMode"]);

            string Username = ConfigurationManager.AppSettings["Username"];
            string WebBrowser = ConfigurationManager.AppSettings["WebBrowser"];
            string FileName = ConfigurationManager.AppSettings["DebugFileName"];
            string LaunchType = ConfigurationManager.AppSettings["LaunchType"].ToLower();

            int TestCaseID = 3713; // Only used for debugging 
            int NotificationLevel = 1;

            if (Debug == false || args.Length > 0)
            {
                if (args.Length == 7 || args.Length == 3)
                {
                    WinControl.MinThis("SW_Automation");
                    string TestID = args[0].ToString();
                    string TestMapID = args[1].ToString();
                    string ThreadID = args[2].ToString();
                    //string TestID = "6534";
                    //string TestMapID = "1092";
                    //string ThreadID = "5298";

                    //RC.StartupGridMode(TestID, TestMapID, ThreadID);

                }
                else
                {
                    Console.WriteLine("Error Please use the following Form:");
                    Console.WriteLine(" Arguments: \"Filename\" \"Current User\"  \"Web Browser\"  \"Host\" \"Port\" \"TestCaseID\" \"Notification Level: 0=All, 1=Failures Only, 2=Start Email and Failures\"");
                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine("Press enter to close.");
                    Console.ReadLine();
                }
            }
            else
            {
                if (LaunchType == "flatfile")
                {
                    //RF.StartDebug(FileName, WebBrowser, Username);
                }

                if (LaunchType == "code")
                {
                    string dllName = ConfigurationManager.AppSettings["dllName"]; // This is where you test resides in your solution explorer
                    string className = ConfigurationManager.AppSettings["className"]; // This is what you named your class file
                    string methodName = ConfigurationManager.AppSettings["methodName"]; // This is the method that you declared for your test

                    string Tenant = ConfigurationManager.AppSettings["TenantID"];
                    string Password = ConfigurationManager.AppSettings["Password"];
                    string Environment = ConfigurationManager.AppSettings["Environment"];

                    if (Environment == "")
                    {
                        Environment = "QA";
                    }

                    string PW = DecryptPW("StayWellPass123", Password, true);

                    //RC.StartUpNonGrid(dllName, className, methodName, WebBrowser, Environment, Tenant, Username, PW);

                    Console.WriteLine();
                    Console.WriteLine("Press enter key to exit.");
                    Console.ReadLine();
                }
            }
        }
    }
}
