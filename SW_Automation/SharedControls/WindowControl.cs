﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace SharedControls
{
    public class WindowControl
    {
        private const int SW_Minimize = 6;
        private const int SW_Maximize = 3;

        [DllImport("User32")]

        private static extern int ShowWindow(int hwnd, int nCmdShow);
        //private static extern int MaxWindow(int hwnd2, int nCmdShow2);

        public void MinThis(string WindowName)
        {
            int hWnd;
            Process[] processRunning = Process.GetProcesses();
            foreach (Process pr in processRunning)
            {
                if (pr.ProcessName == WindowName)
                {
                    hWnd = pr.MainWindowHandle.ToInt32();
                    ShowWindow(hWnd, SW_Minimize);
                }
            }
        }

        public void MaxThis(string WindowName)
        {
            int hWnd;
            Process[] processRunning = Process.GetProcesses();
            foreach (Process pr in processRunning)
            {
                if (pr.ProcessName == WindowName)
                {
                    hWnd = pr.MainWindowHandle.ToInt32();
                    ShowWindow(hWnd, SW_Maximize);
                }
            }
        }
    }
}

